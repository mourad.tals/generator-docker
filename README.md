# Laravel Project: Generator

This project is a Laravel-based application, containerized using Docker for easy setup and development.

## Requirements

- Docker and Docker-compose installed on your machine. If you don't have them installed, you can download Docker [here](https://www.docker.com/get-started) and follow the instructions.

## Setup

1. Clone the repository: 

```bash
git clone https://gitlab.com/mourad.tals/generator-docker.git
```

2. Navigate to the project directory:

```bash
cd generator
```

3. Rename .env.example => .env


4. Build and run the Docker containers:

```bash
docker-compose up -d
```

After running this command, Docker will download the necessary images (if not downloaded previously), build the images for your services based on your Dockerfile and docker-compose.yml configuration, and start the services.


## Application

After the Docker containers are up and running, you can access the application in your browser:


[http://localhost:8000](http://localhost:8000)


## Database

The application uses MySQL for data storage. The MySQL server can be accessed at localhost on port 3306 (e.g., when setting up your application's database connection).

## phpMyAdmin

phpMyAdmin is set up to run on port 8080. You can access it in your browser:

[http://localhost:8080](http://localhost:8080)

Log in with username root and password secret, as set in the docker-compose file.

You should now see the generator database, which is used by your Laravel application.

## Note

If you make changes to the Dockerfile or docker-compose file, you may need to rebuild the images:

```bash
docker-compose up --build -d
```

## Troubleshooting

If you encounter any issues, check the logs of the Docker containers:

```bash
docker-compose logs
```

You can check the logs of a specific service like so:

```bash
docker-compose logs <service-name>
```

For example:

```bash
docker-compose logs generator
```