<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
    
   >
    <title>
        Quicky
    </title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/vendors/vendors.min.css" />
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/themes/vertical-dark-menu-template/materialize.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/themes/vertical-dark-menu-template/style.css" />

    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/custom/custom.css" />

    <link rel="stylesheet" type="text/css" href="/assets/vendors/hover-effects/media-hover-effects.css">

    <link rel="stylesheet" type="text/css" href="/assets/vendors/data-tables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css"
        href="/assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/data-tables/css/dataTables.checkboxes.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/fullcalendar/css/fullcalendar.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/fullcalendar/daygrid/daygrid.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/fullcalendar/timegrid/timegrid.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/pages/app-calendar.min.css">


    <link rel="stylesheet" href="/assets/vendors/select2/select2.min.css" type="text/css">
    <link rel="stylesheet" href="/assets/vendors/select2/select2-materialize.css" type="text/css">


    <link rel="stylesheet" type="text/css" href="/assets/vendors/quill/katex.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/quill/monokai-sublime.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/quill/quill.snow.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendors/quill/quill.bubble.css">

    <!-- END: Custom CSS-->

    @yield('css')
</head>
<!-- END: Head-->

<style>
.sidenav li>a>i.material-icons,
.sidenav li a.collapsible-header>i.material-icons {
    font-size: 24px;
}

input[type=number] {
    height: 30px;
    line-height: 30px;
    font-size: 16px;
    padding: 0 8px;
}

input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    cursor: pointer;
    display: block;
    width: 8px;
    color: #333;
    text-align: center;
    position: relative;

}

input[type=number]::-webkit-inner-spin-button {
    opacity: 1;
    background: #eee url('/assets/images/icon/YYySO.png') no-repeat 50% 50%;
    width: 14px;
    height: 14px;
    padding: 4px;
    position: relative;
    right: 4px;
    border-radius: 28px;
}

.select2-container--default .select2-selection--single .select2-selection__arrow b {
    border-color: rgb(25 145 145) transparent transparent;
}
</style>

<body class="
            vertical-layout
            page-header-light
            vertical-menu-collapsible vertical-dark-menu
            preload-transitions
            2-columns
        " data-open="click" data-menu="vertical-dark-menu" data-col="2-columns">
    <!-- BEGIN: Header-->
    <header class="page-topbar" id="header">
        <div class="navbar navbar-fixed">
            <nav class="
            navbar-main navbar-color nav-collapsible navbar-light nav-collapsed
                    ">
                <div class="nav-wrapper">
                    <div class="header-search-wrapper hide-on-med-and-down">
                        <i class="material-icons">search</i>
                            <input class="header-search-input z-depth-2" type="text" name="exp" placeholder="Recherche"
                                data-search="template-list" />
                        <ul class="search-list collection display-none"></ul>
                    </div>
                    <ul class="navbar-list right valign-wrapper">

                        <li class="hide-on-med-and-down">
                            <a class="
                                        waves-effect waves-block waves-light
                                        toggle-fullscreen
                                    " href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a>
                        </li>
                        <li class="hide-on-large-only search-input-wrapper">
                            <a class="
                                        waves-effect waves-block waves-light
                                        search-button
                                    " href="javascript:void(0);"><i class="material-icons">search</i></a>
                        </li>
                        <li>
                            <a class="" href=""><i class="material-icons tooltipped"
                                    data-position="bottom" data-tooltip="Nouveaux inscris">highlight<small
                                        class="notification-badge"><span id="new_subscribers">0</small></i></a>
                        </li>
                        @if(auth()->user()->role == '1' || auth()->user()->role == '8')
                        <li>
                            <a class="" href=""><i class="material-icons tooltipped"
                                    data-position="bottom" data-tooltip="Reclamations">announcement<small
                                        class="notification-badge"><span id="new_reclamation">0</small></i></a>
                        </li>
                        @endif
                        @if(auth()->user()->role == '1' || auth()->user()->role == '7')
                        <li>
                            <a class="" href=""><i class="material-icons tooltipped"
                                    data-position="bottom" data-tooltip="Ramassages">notifications<small
                                        class="notification-badge"><span id="new_ramassage">0</small></i></a>
                        </li>
                        @endif
                        <li>

                            <a class="
                                        waves-effect waves-block waves-light
                                        profile-button
                                    " href="javascript:void(0);" data-target="profile-dropdown"
                                style="margin-bottom: 10px;"><span class="avatar-status avatar-online"><img
                                        src="{{ Auth::user()->photo ? '/uploads/photos/' . Auth::user()->photo : '/uploads/photos/default.png' }}"
                                        alt="avatar" /><i></i></span></a>

                        </li>

                    </ul>
                    <!-- translation-button-->
                    <ul class="dropdown-content" id="translation-dropdown">

                        <li class="dropdown-item">
                            <a class="grey-text text-darken-1" href="#!" data-language="fr"><i
                                    class="flag-icon flag-icon-fr"></i>
                                Français</a>
                        </li>

                    </ul>
                    <!-- notifications-dropdown-->
                    <ul class="dropdown-content" id="notifications-dropdown">
                        <li>
                            <h6>
                                NOTIFICATIONS<span class="new badge">5</span>
                            </h6>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="black-text" href="#!"><span class="
                                            material-icons
                                            icon-bg-circle
                                            cyan
                                            small
                                        ">add_shopping_cart</span>
                                A new order has been placed!</a>
                            <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">2 hours
                                ago
                            </time>
                        </li>
                        <li>
                            <a class="black-text" href="#!"><span class="
                                            material-icons
                                            icon-bg-circle
                                            red
                                            small
                                        ">stars</span>
                                Completed the task</a>
                            <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">3 days
                                ago
                            </time>
                        </li>
                        <li>
                            <a class="black-text" href="#!"><span class="
                                            material-icons
                                            icon-bg-circle
                                            teal
                                            small
                                        ">settings</span>
                                Settings updated</a>
                            <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">4 days
                                ago
                            </time>
                        </li>
                        <li>
                            <a class="black-text" href="#!"><span class="
                                            material-icons
                                            icon-bg-circle
                                            deep-orange
                                            small
                                        ">today</span>
                                Director meeting started</a>
                            <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">6 days
                                ago
                            </time>
                        </li>
                        <li>
                            <a class="black-text" href="#!"><span class="
                                            material-icons
                                            icon-bg-circle
                                            amber
                                            small
                                        ">trending_up</span>
                                Generate monthly report</a>
                            <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">1 week
                                ago
                            </time>
                        </li>
                    </ul>
                    <!-- profile-dropdown   -->
                    <ul class="dropdown-content" id="profile-dropdown" style="width: 260px !important;">

                        <li><a class="grey-text text-darken-1" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="material-icons">keyboard_tab</i>
                                Se déconnecter</a></li>
                        {{--
                        <li><a class="grey-text text-darken-1" href="{{ route('user_profil') }}"><i
                            class="material-icons"></i>

                        <i class="material-icons">person_outline</i> Mon profil</a></li> --}}


                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>


                    </ul>


                </div>
                <nav class="display-none search-sm">
                    <div class="nav-wrapper">
                        <form id="navbarForm">
                            <div class="input-field search-input-sm">
                                <input class="search-box-sm mb-0" type="search" required="" id="search"
                                    placeholder="Recherche" data-search="template-list" />
                                <label class="label-icon" for="search"><i class="
                                                material-icons
                                                search-sm-icon
                                            ">search</i></label><i class="material-icons search-sm-close">close</i>
                                <ul class="
                                            search-list
                                            collection
                                            search-list-sm
                                            display-none
                                        ">
                                </ul>
                            </div>
                        </form>
                    </div>
                </nav>
            </nav>
        </div>
    </header>
    <!-- END: Header-->
    <ul class="display-none" id="default-search-main">
        <li class="auto-suggestion-title">
            <a class="collection-item" href="#">
                <h6 class="search-title">FILES</h6>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="collection-item" href="#">
                <div class="display-flex">
                    <div class="display-flex align-item-center flex-grow-1">
                        <div class="avatar">
                            <img src="/assets/images/icon/pdf-image.png" width="24" height="30" alt="sample image" />
                        </div>
                        <div class="member-info display-flex flex-column">
                            <span class="black-text">Two new item submitted</span><small class="grey-text">Marketing
                                Manager</small>
                        </div>
                    </div>
                    <div class="status">
                        <small class="grey-text">17kb</small>
                    </div>
                </div>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="collection-item" href="#">
                <div class="display-flex">
                    <div class="display-flex align-item-center flex-grow-1">
                        <div class="avatar">
                            <img src="/assets/images/icon/doc-image.png" width="24" height="30" alt="sample image" />
                        </div>
                        <div class="member-info display-flex flex-column">
                            <span class="black-text">52 Doc file Generator</span><small class="grey-text">FontEnd
                                Developer</small>
                        </div>
                    </div>
                    <div class="status">
                        <small class="grey-text">550kb</small>
                    </div>
                </div>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="collection-item" href="#">
                <div class="display-flex">
                    <div class="display-flex align-item-center flex-grow-1">
                        <div class="avatar">
                            <img src="/assets/images/icon/xls-image.png" width="24" height="30" alt="sample image" />
                        </div>
                        <div class="member-info display-flex flex-column">
                            <span class="black-text">25 Xls File Uploaded</span><small class="grey-text">Digital
                                Marketing Manager</small>
                        </div>
                    </div>
                    <div class="status">
                        <small class="grey-text">20kb</small>
                    </div>
                </div>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="collection-item" href="#">
                <div class="display-flex">
                    <div class="display-flex align-item-center flex-grow-1">
                        <div class="avatar">
                            <img src="/assets/images/icon/jpg-image.png" width="24" height="30" alt="sample image" />
                        </div>
                        <div class="member-info display-flex flex-column">
                            <span class="black-text">Anna Strong</span><small class="grey-text">Web
                                Designer</small>
                        </div>
                    </div>
                    <div class="status">
                        <small class="grey-text">37kb</small>
                    </div>
                </div>
            </a>
        </li>
        <li class="auto-suggestion-title">
            <a class="collection-item" href="#">
                <h6 class="search-title">MEMBERS</h6>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="collection-item" href="#">
                <div class="display-flex">
                    <div class="display-flex align-item-center flex-grow-1">
                        <div class="avatar">
                            <img class="circle" src="/assets/images/avatar/avatar-7.png" width="30"
                                alt="sample image" />
                        </div>
                        <div class="member-info display-flex flex-column">
                            <span class="black-text">John Doe</span><small class="grey-text">UI
                                designer</small>
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="collection-item" href="#">
                <div class="display-flex">
                    <div class="display-flex align-item-center flex-grow-1">
                        <div class="avatar">
                            <img class="circle" src="/assets/images/avatar/avatar-8.png" width="30"
                                alt="sample image" />
                        </div>
                        <div class="member-info display-flex flex-column">
                            <span class="black-text">Michal Clark</span><small class="grey-text">FontEnd
                                Developer</small>
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="collection-item" href="#">
                <div class="display-flex">
                    <div class="display-flex align-item-center flex-grow-1">
                        <div class="avatar">
                            <img class="circle" src="/assets/images/avatar/avatar-10.png" width="30"
                                alt="sample image" />
                        </div>
                        <div class="member-info display-flex flex-column">
                            <span class="black-text">Milena Gibson</span><small class="grey-text">Digital
                                Marketing</small>
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="collection-item" href="#">
                <div class="display-flex">
                    <div class="display-flex align-item-center flex-grow-1">
                        <div class="avatar">
                            <img class="circle" src="/assets/images/avatar/avatar-12.png" width="30"
                                alt="sample image" />
                        </div>
                        <div class="member-info display-flex flex-column">
                            <span class="black-text">Anna Strong</span><small class="grey-text">Web
                                Designer</small>
                        </div>
                    </div>
                </div>
            </a>
        </li>
    </ul>
    <ul class="display-none" id="page-search-title">
        <li class="auto-suggestion-title">
            <a class="collection-item" href="#">
                <h6 class="search-title">PAGES</h6>
            </a>
        </li>
    </ul>
    <ul class="display-none" id="search-not-found">
        <li class="auto-suggestion">
            <a class="collection-item display-flex align-items-center" href="#"><span
                    class="material-icons">error_outline</span><span class="member-info">No results
                    found.</span></a>
        </li>
    </ul>

    <!-- BEGIN: SideNav-->
    <aside class="sidenav-main nav-collapsible sidenav-dark sidenav-active-rounded nav-collapsed">
        <div class="brand-sidebar">
            <h1 class="logo-wrapper">
                <a class="brand-logo darken-1" href="{{ route('admin') }}"><img class="hide-on-med-and-down"
                        src="/assets/images/logo/materialize-logo2.png" alt="materialize logo" /><img
                        class="show-on-medium-and-down hide-on-med-and-up"
                        src="/assets/images/logo/materialize-logo-color.png" alt="materialize logo" /><span
                        class="logo-text hide-on-med-and-down">Quicky</span></a><a class="navbar-toggler" href="#"><i
                        class="material-icons">radio_button_unchecked</i></a>
            </h1>
        </div>
        <ul class="
                    sidenav sidenav-collapsible
                    leftside-navigation
                    collapsible
                    sidenav-fixed
                    menu-shadow ps ps--active-y
                " id="slide-out" data-menu="menu-navigation" data-collapsible="accordion" style="padding-top: 5px;">

                @foreach ($menus as $menu)

                    @if(!is_numeric($menu->parent_menu))
                    @php($subMenus = $menu->getSubMenus($menu->id))
                    @if($subMenus->count()==0)
                    @if($menu->ressource == "" || \Auth::user()::hasRessource($menu->ressource_name))
                    <li class="bold">
                        <a class="{{ in_array(Route::currentRouteName(), [$menu->page]) ? 'active' : '' }}"
                            href="{{route($menu->page ?? 'admin')}}"><i class="material-icons">{{$menu->icon}}</i><span
                                class="menu-title">
                                {{$menu->titre}}</span></a>
                    </li>
                    @endif
                    @else
                    @if($menu->ressource == "" || \Auth::user()::hasRessource($menu->ressource_name))
                    <li class="bold">
                        <a class="collapsible-header waves-effect waves-cyan" href="JavaScript:void(0)"><i
                                class="material-icons">{{$menu->icon}}</i><span class="menu-title">
                                {{$menu->titre}} </span></a>
                        <div class="collapsible-body">
                            <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                                @foreach($subMenus as $subMenu)
                                @if($subMenu->ressource == "" || \Auth::user()::hasRessource($subMenu->ressource_name))
                                <li class="bold">
                                    <a class="waves-effect waves-cyan" href="{{route($subMenu->page ?? 'admin')}}"><i
                                            class="material-icons" style="font-size: 1.4rem;">{{$subMenu->icon}}</i><span
                                            class="menu-title">
                                            {{$subMenu->titre}}</span></a>
                                </li>
                                @endif
                                @endforeach
                            </ul>
                        </div>
                    </li>
                    @endif
                    @endif
                    @endif
                    @endforeach

        </ul>
        <div class="navigation-background"></div>
        <a class="
                    sidenav-trigger
                    btn-sidenav-toggle btn-floating btn-medium
                    waves-effect waves-light
                    hide-on-large-only
                " href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
    </aside>
    <!-- END: SideNav-->

    <!-- BEGIN: Page Main-->
    <div id="main" class="main-full">
        @yield('content')
    </div>
    <!-- END: Page Main-->

    <!-- BEGIN: Footer-->

    <footer class="
                page-footer
                footer footer-static footer-light
                navbar-border navbar-shadow
            ">
        <div class="footer-copyright">
            <div class="container">
                <span>&copy; <?php echo date('Y'); ?>
                    <a href="#" target="_blank">TAWSSIL</a>
                    All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by
                    <a href="#">TAWSSIL</a></span>
            </div>
        </div>
    </footer>

    <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <script src="/assets/js/vendors.min.js"></script>


    <script src="/assets/vendors/quill/katex.min.js"></script>
    <script src="/assets/vendors/quill/highlight.min.js"></script>
    <script src="/assets/vendors/quill/quill.min.js"></script>


    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="/assets/vendors/chartjs/chart.min.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="/assets/js/plugins.js"></script>
    <script src="/assets/js/search.js"></script>
    <script src="/assets/js/custom/custom-script.js"></script>

    <script src="/assets/vendors/data-tables/js/jquery.dataTables.min.js"></script>
    <script src="/assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script>
    <script src="/assets/vendors/data-tables/js/datatables.checkboxes.min.js"></script>

    <script src="/assets/vendors/select2/select2.full.min.js"></script>

    <script src="/assets/js/vue3.prod.js"></script>


    <!-- END THEME  JS-->

    <!-- BEGIN PAGE LEVEL JS
     -->
    <!-- END PAGE LEVEL JS-->


    <script>
    $(document).ready(function() {
        // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
        $('.modal').modal();
    })
    $(document).ready(function() {




        $('.timepicker').timepicker({
            twelveHour: false,
            showClearBtn: true,
            autoClose: true,
            showView: 'hours',
            i18n: {
                months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août',
                    'Septembre', 'Octobre', 'Novembre', 'Décembre'
                ],

                monthsShort: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jui', 'Jui', 'Aoû', 'Sep', 'Oct',
                    'Nov', 'Déc'
                ],
                weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
                today: 'Aujourd\'hui',
                cancel: 'Annuler',
                done: 'OK',
                clear: 'Effacer'
            }
        });
        $('.datepicker').datepicker({
            firstDay: true,
            format: 'yyyy-mm-dd',
            clear: 'effacer',
            formatSubmit: 'yyyy/mm/dd',
            showClearBtn: true,
            autoClose: true,
            i18n: {
                months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août',
                    'Septembre', 'Octobre', 'Novembre', 'Décembre'
                ],
                monthsShort: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jui', 'Jui', 'Aoû', 'Sep', 'Oct',
                    'Nov', 'Déc'
                ],
                weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
                today: 'Aujourd\'hui',
                cancel: 'Annuler',
                done: 'OK',
                clear: 'Effacer'
            }
        });
        $(".select2").select2({
            dropdownAutoWidth: true,
            width: '100%'
        });


        


        const App = {
            mounted() {
                this.loadData();
                $('.tooltipped').tooltip();
            },
            methods: {
                loadData() {
                    if ($("#list-datatable").length > 0) {
                        $("#list-datatable").DataTable({
                            "aaSorting": [
                                [0, "desc"]
                            ],
                            "language": {
                                url: '/assets/vendors/data-tables/i18n/fr_fr.json'
                            }
                        });
                    };
                },
            },
        }
        Vue.createApp(App).mount('#app');
    });
    </script>

    @yield('js')

</body>

</html>
