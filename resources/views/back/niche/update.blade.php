@extends($layout)
@section('content')
<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">

                <form method="POST" action="{{route('niche_update', ['niche' => $record->id])}}" enctype="multipart/form-data">
                    @csrf
                    <br>
                    @if (Session::has('success'))
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p>{{ Session::get('success') }} </p>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    @endif
                    <div class="card">
                        <div class="card-panel">
                        <div class="row">
                                <div class="col s12 m12">
                                    <div class="row">
                                                        
                                        <div class="col s6 input-field">
                                            <input id="label" name="label" value="{{old('label', $record->label)}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="label"> Libellé </label>
                                            @error('label')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s12 input-field">
                                            <textarea d="desc" name="desc" class="materialize-textarea">{{old('desc', $record->desc)}}</textarea>
                                            <label for="desc"> Description </label>
                                            @error('desc')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s12 input-field">
                                            <textarea d="concurrents" name="concurrents" class="materialize-textarea">{{old('concurrents', $record->concurrents)}}</textarea>
                                            <label for="concurrents"> Exemple concurrents </label>
                                            @error('concurrents')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s6 input-field">
                                            <input id="title" name="title" value="{{old('title', $record->title)}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="title"> title </label>
                                            @error('title')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s6 input-field">
                                            <input id="subtitle" name="subtitle" value="{{old('subtitle', $record->subtitle)}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="subtitle">  subtitle </label>
                                            @error('subtitle')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s6 input-field">
                                            <div class="file-field input-field" style="width: 100%; float: left;">
                            <div class="btn">
                                <span>Icone</span>
                                <input name="file" type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path" type="text">
                            </div>
                        </div>
                                            </div>
                                    <div class="col s6 input-field">
                                    <select name='status' id='status' class="select2 browser-default">
                                    <option value=''></option>
                                        @foreach ($statutRecords as $row)
                                            <option class='option' {{($row->key == old('status', $record->status)) ? 'selected' : ''}}
                                            value='{{$row->key}}'> {{$row->value}}</option>
                                        @endforeach
                                </select>
                                    <label for="status"> Statut</label>
                                    @error('status')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                    @enderror
                                </div>
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12">
                                    <div class="col s12 display-flex justify-content-end mt-3">
                                        <a href="{{route('niche_list')}}"><button type="button"
                                                class="btn btn-light">Retour </button></a>
                                        <button type="submit" class="btn indigo" style="margin-left: 1rem;">
                                            Enregistrer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop