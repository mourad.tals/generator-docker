@extends($layout)
@section('css')
<link rel="stylesheet" type="text/css" href="/assets/css/pages/page-users.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
@stop
@section('content')
<div id="breadcrumbs-wrapper" data-image="/assets/images/gallery/breadcrumb-bg.jpg" class="breadcrumbs-bg-image">
    <!-- Search for small screen-->
    <div class="container">
        <div class="row">
            <div class="col s12 m6 l6">
                <h2 class="flow-text white-text mt-0 mb-0 valign-wrapper"><i
                        class="material-icons mr-3">people_outline</i><span>Gestion des utilisateurs</span></h2>
            </div>
            <div class="col s12 m6 l6 right-align-md">
                <ol class="breadcrumbs mb-0">
                    <li class="breadcrumb-item"><a href="{{route('admin')}}">Accueil</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{route('user_list')}}">Liste des utilisateurs</a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">
                <br>
                @if (\Session::has('success'))
                <div class="card-alert card gradient-45deg-green-teal">
                    <div class="card-content white-text">
                        <p>
                            <i class="material-icons">check</i> {!! \Session::get('success') !!}
                        </p>
                    </div>
                    <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                @endif
                <form id="form" method="POST">
                    @csrf

                    <div class="row">

                        <div class="col s12 m4 input-field">
                            <select id="role" name="role" class="select select2 browser-default">
                                <option value='0'>Toutes les role</option>
                                @foreach ($roles as $row)
                                    <option class='option' value='{{ $row->id }}'> {{ $row->label }}</option>
                                @endforeach
                            </select>
                            <label for="role">Filtrer par Role</label>
                        </div>

                        <div class="col s12 m4 input-field">
                        <button type="submit" class="btn btn-light" style="margin-right: 1rem;">
                            <i class="material-icons">search</i></button>
                        </div>
                    </div>

                    <!-- Dropdown Structure -->


                </form>
                <div class="users-list-table" id="app">

                    <div class="card">
                        <div class="card-content">
                            <!-- datatable start -->
                            <div class="responsive-table">
                                <table id="list-datatable" style="width: 100%!important;"
                                    class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Photo</th>
                                            <th>Nom</th>
                                            <th class="hide-on-small-only">Identifiant</th>
                                            <th class="hide-on-small-only">Role</th>
                                            <th class="hide-on-small-only">Date de création</th>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($users as $user)

                                        <tr>
                                            <td>
                                                <img src="{{$user->photo ? '/uploads/photos/'.Auth::user()->photo : '/uploads/photos/default.png' }}"
                                                    class="border-radius-4" alt="profile image" height="32"
                                                    width="32" />
                                            </td>
                                            <td>{{ $user->name.' '.$user->first_name }}
                                                <p class="hide-on-med-and-up"> <span
                                                        class="new badge gradient-45deg-light-blue-cyan"
                                                        data-badge-caption="{{ $user->role_label}}"></span>
                                                </p>
                                            </td>
                                            <td class="hide-on-small-only"> {{ $user->login }} </td>
                                            <td class="hide-on-small-only">{{ $user->role_label}}</td>
                                            <td class="hide-on-small-only">{{ $user->created_at}}</td>
                                            <td>

                                                <a href="{{route('user_update', ['user'=>$user->id])}}"><i
                                                        class="material-icons tooltipped" data-position="top"
                                                        data-tooltip="Modifier">edit</i></a>
                                                <a href="#!" onclick="openSuppModal({{$user->id}})"><i
                                                        class="material-icons tooltipped" style="color: #c10027;"
                                                        data-position="top" data-tooltip="Supprimer">delete</i></a>
                                            </td>

                                        </tr>
                                        @endforeach

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
            <div style="bottom: 50px; right: 19px;" class="fixed-action-btn direction-top"><a
                    href="{{route('user_create')}}"
                    class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow"><i
                        class="material-icons">add</i></a>
            </div>
        </div>
        <div class="content-overlay"></div>
    </div>
</div>

<div id="delete_modal" class="modal">
    <div class="modal-content">
        <h4> Confirmation de suppression</h4>
        <div>
            Êtes-vous sûr de vouloir supprimer ?
        </div>
        <input type="hidden" name="delId" id="delId">
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn grey">Annuler</a>
        <a href="#!" class="waves-effect waves-green btn red" onclick="suppRecord()">Supprimer</a>
    </div>
</div>
@stop
@section('js')
<script src="/assets/js/scripts/page-users.js"></script>
<script>
function openSuppModal(id) {
    $("#delId").val(id);
    $('#delete_modal').modal('open');
}

function suppRecord() {
    window.location.replace("/user/delete/" + $("#delId").val());
}
$(document).ready(function() {
    $('.modal').modal();
});
</script>
@stop
