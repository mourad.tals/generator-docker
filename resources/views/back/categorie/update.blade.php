@extends($layout)
@section('content')
<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">

                <form method="POST" action="{{route('categorie_update', ['categorie' => $record->id])}}">
                    @csrf
                    <br>
                    @if (Session::has('success'))
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p>{{ Session::get('success') }} </p>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    @endif


                    <div class="row">
                        <div class="col s12">
                          <ul class="tabs">
                            <li class="tab col s3"><a href="#generale">Générale</a></li>
                            <li class="tab col s3"><a href="#ads">Config ADS</a></li>
                          </ul>
                        </div>

                        <div id="generale" class="col s12">


                    <div class="card">
                        <div class="card-panel">
                        <div class="row">
                                <div class="col s12 m12">
                                    <div class="row">
                                        
                                    <div class="col s6 input-field">
                                    <select name='application' id='application' class="select2 browser-default">
                                    <option value=''></option>
                                        @foreach ($applicationRecords as $row)
                                            <option class='option' {{($row->id == old('application', $record->application)) ? 'selected' : ''}}
                                            value='{{$row->id}}'> {{$row->label}}</option>
                                        @endforeach
                                </select>
                                    <label for="application"> Application</label>
                                    @error('application')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                    @enderror
                                </div>
                                                    
                                        <div class="col s6 input-field">
                                            <input id="label" name="label" value="{{old('label', $record->label)}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="label"> Libellé </label>
                                            @error('label')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s6 input-field">
                                            <input id="ordre" name="ordre" value="{{old('ordre', $record->ordre)}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="ordre"> Ordre </label>
                                            @error('ordre')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    <div class="col s6 input-field">
                                    <select name='statut' id='statut' class="select2 browser-default">
                                    <option value=''></option>
                                        @foreach ($statutRecords as $row)
                                            <option class='option' {{($row->key == old('statut', $record->statut)) ? 'selected' : ''}}
                                            value='{{$row->key}}'> {{$row->value}}</option>
                                        @endforeach
                                </select>
                                    <label for="statut"> Statut</label>
                                    @error('statut')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                    @enderror
                                </div>
                                                    
                                        <div class="col s12 input-field">
                                            <textarea d="desc" name="desc" class="materialize-textarea">{{old('desc', $record->desc)}}</textarea>
                                            <label for="desc"> Description </label>
                                            @error('desc')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div>


                            </div>
                     <div id="ads" class="col s12">

                        <div class="card" >
                        <div class="card-panel">
                        <div class="row">
                                <div class="col s12 m12">
                                    <div class="row">



                                        




                                        <div class="col s6">
                                            <table class="striped">
                                              <thead>
                                                <tr>
                                                    <th></th>
                                                    @foreach($typeads as $ads)
                                                        <th>{{$ads->label}}</th>
                                                    @endforeach
                                                </tr>
                                              </thead>
                                              <tbody>
                                                @foreach($adnetworks as $nt)
                                                    <tr>
                                                        <th>{{$nt->label}}</th>

                                                        @foreach($typeads as $ads)
                                                            <td style="text-align: center;"> 
                                                                <label>
                                                                    <input type="checkbox" name="perm_ads[{{$nt->id.'_'.$ads->id}}]" {{$row->id}}' {{(in_array($nt->id.'_'.$ads->id,$adsperm)) ? 'checked' : ''}} />
                                                                    <span></span>
                                                                  </label>
                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                              </tbody>
                                            </table>
                                        </div>

                                        


                                    </div></div></div></div></div>



                            
                        </div>
                    </div>


                    <div class="row">
                        <div class="col s12 m12">
                            <div class="col s12 display-flex justify-content-end mt-3">
                                <a href="{{route('categorie_list')}}"><button type="button"
                                        class="btn btn-light">Retour </button></a>
                                <button type="submit" class="btn indigo" style="margin-left: 1rem;">
                                    Enregistrer</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop