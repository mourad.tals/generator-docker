@extends($layout)

@section('content')

<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">
                <form method="POST" action="{{route('apparence_create')}}">
                    @csrf
                    <br>
                    <div class="card">
                        <div class="card-panel">
                            <div class="row">
                                <div class="col s12 m12">
                                    <div class="row">
                                        <div class="col s6 input-field">
                                            <select name='layout' id='layout' class="select2 browser-default">
                                                <option value="horizontal"> Horizontal </option> 
                                                <option value="vertical"> Vertical </option> 
                                                <option value="verticalmoderne"> Vertical moderne </option> 
                                                </select> <label for="layout"> Layout</label>
                                            @error('layout')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                                        
                                        <div class="col s6 input-field">
                                            <input id="label" name="label" value="{{old('label')}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="label"> Libellé </label>
                                            @error('label')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div> 
                                                       
                                        <div class="col s6 input-field">
                                            <input id="title" name="title" value="{{old('title')}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="title"> Titre </label>
                                            @error('title')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s6 input-field">
                                            <input id="logo_titre" name="logo_titre" value="{{old('logo_titre')}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="logo_titre"> logo_titre </label>
                                            @error('logo_titre')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    
                                                    
                                        <div class="col s6 input-field">
                                            <input id="logo" name="logo" value="{{old('logo')}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="logo"> Logo </label>
                                            @error('logo')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s6 input-field">
                                            <input id="logo_home" name="logo_home" value="{{old('logo_home')}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="logo_home"> Logo menu </label>
                                            @error('logo_home')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s6 input-field">
                                            <input id="couleur_header" name="couleur_header" value="{{old('couleur_header')}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="couleur_header"> Couleur header </label>
                                            @error('couleur_header')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s6 input-field">
                                            <input id="couleur_sidebar" name="couleur_sidebar" value="{{old('couleur_sidebar')}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="couleur_sidebar"> Couleur sidebar </label>
                                            @error('couleur_sidebar')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s6 input-field">
                                            <input id="couleur_sidebar_logo" name="couleur_sidebar_logo" value="{{old('couleur_sidebar_logo')}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="couleur_sidebar_logo"> Couleur sidebar logo </label>
                                            @error('couleur_sidebar_logo')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s6 input-field">
                                            <input id="couleur_menu_actif" name="couleur_menu_actif" value="{{old('couleur_menu_actif')}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="couleur_menu_actif"> Couleur menu actif </label>
                                            @error('couleur_menu_actif')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    <div class="col s6 input-field">
                                    <select name='statut' id='statut' class="select2 browser-default">
                                        <option value="1"> Oui </option> 
                                        <option value="0"> Non </option> 
                                        </select> <label for="statut"> Statut</label>
                                    @error('statut')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                    @enderror
                                </div>
                                    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12">
                                    <div class="col s12 display-flex justify-content-end mt-3">

                                        <button type="submit" class="btn indigo" style="margin-left: 1rem;">
                                            تسجيل</button>

                                        <a href="{{route('apparence_list')}}"><button type="button"
                                                class="btn btn-light">رجوع </button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop