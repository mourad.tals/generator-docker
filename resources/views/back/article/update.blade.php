@extends($layout)
@section('content')
<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">

                <form method="POST" action="{{route('article_update', ['article' => $record->id])}}" enctype="multipart/form-data">
                    @csrf
                    <br>
                    @if (Session::has('success'))
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p>{{ Session::get('success') }} </p>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    @endif


                    <!--<div class="editor">

                    </div> -->

                    <div class="row">
                        <div class="col s12">
                          <ul class="tabs">
                            <li class="tab col s3"><a href="#generale">Générale</a></li>
                            <li class="tab col s3"><a href="#ads">Config ADS</a></li>
                          </ul>
                        </div>

                        <div id="generale" class="col s12">


                    <div class="card">
                        <div class="card-panel">
                        <div class="row">
                                <div class="col s12 m12">
                                    <div class="row">
                                        
                                    
                                    
                                <div class="col s6 input-field">
                                    <select name='cat' id='cat' class="select2 browser-default">
                                    <option value=''></option>
                                        @foreach ($categorieRecords as $row)
                                            <option class='option' {{($row->id == old('cat', $record->cat)) ? 'selected' : ''}}
                                            value='{{$row->id}}'> {{$row->label}}</option>
                                        @endforeach
                                </select>
                                    <label for="cat"> Catégorie</label>
                                    @error('cat')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                    @enderror
                                </div>
                                                    
                                        <div class="col s6 input-field">
                                            <input id="label" name="label" value="{{old('label', $record->label)}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="label"> Libellé </label>
                                            @error('label')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s12 input-field">
                                            <textarea id="contenu1" name="contenu1" style="display: none" class="materialize-textarea"></textarea>
                                             <label for="contenu2"> Contenu 1 </label>
                                             <br><br>
                                            <div class="editor" id="contenu1_editor"> {!! old('contenu1', $record->contenu1 ) !!} </div>
                                        </div>                
                                        <div class="col s12 input-field">
                                            


                                             <textarea id="contenu2" name="contenu2" style="display: none" class="materialize-textarea"></textarea>
                                             <label for="contenu2"> Contenu 2 </label>
                                             <br><br>
                                            <div class="editor" id="contenu2_editor"> {!! old('contenu2', $record->contenu2 ) !!} </div>


                                        </div>
                                    <div class="col s6 input-field">
                                    <select name='statut' id='statut' class="select2 browser-default">
                                    <option value=''></option>
                                        @foreach ($statutRecords as $row)
                                            <option class='option' {{($row->key == old('statut', $record->statut)) ? 'selected' : ''}}
                                            value='{{$row->key}}'> {{$row->value}}</option>
                                        @endforeach
                                </select>
                                    <label for="statut"> Statut</label>
                                    @error('statut')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                    @enderror
                                </div>
                                                    
                                        <div class="col s6 input-field">
                                            <div class="file-field input-field" style="width: 100%; float: left;">
                            <div class="btn">
                                <span>Icone</span>
                                <input name="file" type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path" type="text">
                            </div>
                        </div>
                                            </div>


                                            
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>


                    </div>
                     <div id="ads" class="col s12">

                        <div class="card" >
                        <div class="card-panel">
                        <div class="row">
                                <div class="col s12 m12">
                                    <div class="row">



                                        




                                        <div class="col s6">
                                            <table class="striped">
                                              <thead>
                                                <tr>
                                                    <th></th>
                                                    @foreach($typeads as $ads)
                                                        <th>{{$ads->label}}</th>
                                                    @endforeach
                                                </tr>
                                              </thead>
                                              <tbody>
                                                @foreach($adnetworks as $nt)
                                                    <tr>
                                                        <th>{{$nt->label}}</th>

                                                        @foreach($typeads as $ads)
                                                            <td style="text-align: center;"> 
                                                                <label>
                                                                    <input type="checkbox" name="perm_ads[{{$nt->id.'_'.$ads->id}}]" {{$row->id}}' {{(in_array($nt->id.'_'.$ads->id,$adsperm)) ? 'checked' : ''}} />
                                                                    <span></span>
                                                                  </label>
                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                              </tbody>
                                            </table>
                                        </div>

                                        


                                    </div></div></div></div></div>



                            
                        </div>
                    </div>


                    <div class="row">
                        <div class="col s12 m12">
                            <div class="col s12 display-flex justify-content-end mt-3">
                                <a href="{{route('article_list')}}"><button type="button"
                                        class="btn btn-light">Retour </button></a>
                                <button type="submit" class="btn indigo" style="margin-left: 1rem;">
                                    Enregistrer</button>
                            </div>
                        </div>
                    </div>
                </form>
                </form>
            </div>
        </div>
    </div>
</div>


@stop

@section('js')


<script>
    $(document).ready(function() {

    var contenu1 = new Quill('#contenu1_editor', {
    bounds: ' .editor',
    modules: {
      'formula': true,
      'syntax': true,
      'toolbar': [
        [{
          'font': []
        }, {
          'size': []
        }],
        ['bold', 'italic', 'underline', 'strike'],
        [{
          'color': []
        }, {
          'background': []
        }],
        [{
          'script': 'super'
        }, {
          'script': 'sub'
        }],
        [{
          'header': '1'
        }, {
          'header': '2'
        }, 'blockquote', 'code-block'],
        [{
          'list': 'ordered'
        }, {
          'list': 'bullet'
        }, {
          'indent': '-1'
        }, {
          'indent': '+1'
        }],
        ['direction', {
          'align': []
        }],
        ['link', 'image', 'video', 'formula'],
        ['clean']
      ],
    },
    theme: 'snow'
  });


    contenu1.on('text-change', function(delta, source) {
       document.getElementById('contenu1').innerHTML = contenu1.root.innerHTML;
    });



    var contenu2 = new Quill('#contenu2_editor', {
    modules: {
      'formula': true,
      'syntax': true,
      'toolbar': [
        [{
          'font': []
        }, {
          'size': []
        }],
        ['bold', 'italic', 'underline', 'strike'],
        [{
          'color': []
        }, {
          'background': []
        }],
        [{
          'script': 'super'
        }, {
          'script': 'sub'
        }],
        [{
          'header': '1'
        }, {
          'header': '2'
        }, 'blockquote', 'code-block'],
        [{
          'list': 'ordered'
        }, {
          'list': 'bullet'
        }, {
          'indent': '-1'
        }, {
          'indent': '+1'
        }],
        ['direction', {
          'align': []
        }],
        ['link', 'image', 'video', 'formula'],
        ['clean']
      ],
    },
    theme: 'snow'
  });

    contenu2.on('text-change', function(delta, source) {
       document.getElementById('contenu2').innerHTML = contenu2.root.innerHTML;
    });



    $( "form" ).submit(function( event ) {
      document.getElementById('contenu1').innerHTML = contenu1.root.innerHTML;
      document.getElementById('contenu2').innerHTML = contenu2.root.innerHTML;
    });

    });

</script>

@stop