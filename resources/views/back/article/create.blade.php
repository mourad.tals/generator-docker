@extends($layout)

@section('content')
<div id="breadcrumbs-wrapper" data-image="/assets/images/gallery/breadcrumb-bg.jpg" class="breadcrumbs-bg-image"
    style="background-image: url(/assets/images/gallery/breadcrumb-bg.jpg&quot;);">
    <!-- Search for small screen-->
    <div class="container">
        <div class="row">
            <div class="col s12 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Gestion des articles</span></h5>
            </div>
            <div class="col s12 m6 l6 right-align-md">
                <ol class="breadcrumbs mb-0">
                    <li class="breadcrumb-item"><a href="{{route('admin')}}">Accueil</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{route('article_list')}}">Liste des articles</a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">
                <form method="POST" action="{{route('article_create')}}" enctype="multipart/form-data">
                    @csrf
                    <br>
                    <div class="card">
                        <div class="card-panel">
                            <div class="row">
                                <div class="col s12 m12">
                                    <div class="row">
                                        
                                    <div class="col s6 input-field">
                                    <select name='application' id='application' class="select2 browser-default">
                                    <option value=''></option>
                                    @foreach ($applicationRecords as $row)
                                        <option class='option' {{($row->id == old('application')) ? 'selected' : ''}}
                                        value='{{$row->id}}'> {{$row->label}}</option>
                                    @endforeach
                                </select>
                                    <label for="application"> Application</label>
                                    @error('application')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="col s6 input-field">
                                    <br><br><br>
                                </div>
                                    
                                    <div class="col s6 input-field">
                                    <select name='cat' id='cat' class="select2 browser-default">
                                    <option value=''></option>
                                    @foreach ($categorieRecords as $row)
                                        <option class='option' {{($row->id == old('cat')) ? 'selected' : ''}}
                                        value='{{$row->id}}'> {{$row->label}}</option>
                                    @endforeach
                                </select>
                                    <label for="cat"> Catégorie</label>
                                    @error('cat')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                    @enderror
                                </div>
                                                    
                                        <div class="col s6 input-field">
                                            <input id="label" name="label" value="{{old('label')}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="label"> Libellé </label>
                                            @error('label')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s12 input-field">
                                            

                                            <textarea id="contenu1" name="contenu1" style="display: none" class="materialize-textarea"></textarea>

                                            <label for="contenu1"> Contenu 1 </label>

                                            <br><br>
                                            <div class="editor" id="contenu1_editor"> </div>
                                            
                                        </div>                        
                                        <div class="col s12 input-field">
                                            <textarea id="contenu2" name="contenu2" style="display: none" class="materialize-textarea"></textarea>
                                             <label for="contenu2"> Contenu 2 </label>
                                             <br><br>
                                            <div class="editor" id="contenu2_editor"> </div>
                                        </div>
                                    <div class="col s6 input-field">
                                    <select name='statut' id='statut' class="select2 browser-default">
                                    <option value=''></option>
                                    @foreach ($statutRecords as $row)
                                        <option class='option' {{($row->key == old('statut')) ? 'selected' : ''}}
                                        value='{{$row->key}}'> {{$row->value}}</option>
                                    @endforeach
                                </select>
                                    <label for="statut"> Statut</label>
                                    @error('statut')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                    @enderror
                                </div>
                                                    
                                        



                                <div class="col s6 input-field">
                                            <div class="file-field input-field" style="width: 100%; float: left;">
                            <div class="btn">
                                <span>Icone</span>
                                <input name="file" type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path" type="text">
                            </div>
                        </div>
                                            </div>




                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12">
                                    <div class="col s12 display-flex justify-content-end mt-3">
                                        <a href="{{route('article_list')}}"><button type="button"
                                                class="btn btn-light">Retour </button></a>
                                        <button type="submit" class="btn indigo" style="margin-left: 1rem;">
                                            Enregistrer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')

<script>
    $(document).ready(function() {

    var contenu1 = new Quill('#contenu1_editor', {
    bounds: ' .editor',
    modules: {
      'formula': true,
      'syntax': true,
      'toolbar': [
        [{
          'font': []
        }, {
          'size': []
        }],
        ['bold', 'italic', 'underline', 'strike'],
        [{
          'color': []
        }, {
          'background': []
        }],
        [{
          'script': 'super'
        }, {
          'script': 'sub'
        }],
        [{
          'header': '1'
        }, {
          'header': '2'
        }, 'blockquote', 'code-block'],
        [{
          'list': 'ordered'
        }, {
          'list': 'bullet'
        }, {
          'indent': '-1'
        }, {
          'indent': '+1'
        }],
        ['direction', {
          'align': []
        }],
        ['link', 'image', 'video', 'formula'],
        ['clean']
      ],
    },
    theme: 'snow'
  });


    contenu1.on('text-change', function(delta, source) {
       document.getElementById('contenu1').innerHTML = contenu1.root.innerHTML;
    });



    var contenu2 = new Quill('#contenu2_editor', {
    modules: {
      'formula': true,
      'syntax': true,
      'toolbar': [
        [{
          'font': []
        }, {
          'size': []
        }],
        ['bold', 'italic', 'underline', 'strike'],
        [{
          'color': []
        }, {
          'background': []
        }],
        [{
          'script': 'super'
        }, {
          'script': 'sub'
        }],
        [{
          'header': '1'
        }, {
          'header': '2'
        }, 'blockquote', 'code-block'],
        [{
          'list': 'ordered'
        }, {
          'list': 'bullet'
        }, {
          'indent': '-1'
        }, {
          'indent': '+1'
        }],
        ['direction', {
          'align': []
        }],
        ['link', 'image', 'video', 'formula'],
        ['clean']
      ],
    },
    theme: 'snow'
  });

    contenu2.on('text-change', function(delta, source) {
       document.getElementById('contenu2').innerHTML = contenu2.root.innerHTML;
    });

    });

</script>

@stop