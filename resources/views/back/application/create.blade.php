@extends($layout)

@section('content')
<div id="breadcrumbs-wrapper" data-image="/assets/images/gallery/breadcrumb-bg.jpg" class="breadcrumbs-bg-image"
    style="background-image: url(/assets/images/gallery/breadcrumb-bg.jpg&quot;);">
    <!-- Search for small screen-->
    <div class="container">
        <div class="row">
            <div class="col s12 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Gestion des applications</span></h5>
            </div>
            <div class="col s12 m6 l6 right-align-md">
                <ol class="breadcrumbs mb-0">
                    <li class="breadcrumb-item"><a href="{{route('admin')}}">Accueil</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{route('application_list')}}">Liste des applications</a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">
                <form method="POST" action="{{route('application_create')}}" enctype="multipart/form-data">
                    @csrf
                    <br>
                    <div class="card">
                        <div class="card-panel">
                            <div class="row">
                                <div class="col s12 m12">
                                    <div class="row">
                                                        
                                        <div class="col s12 input-field">
                                            <input id="label" name="label" value="{{old('label')}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="label"> Libellé </label>
                                            @error('label')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    <div class="col s6 input-field">
                                    <select name='type' id='type' class="select2 browser-default">
                                    <option value=''></option>
                                    @foreach ($typeRecords as $row)
                                        <option class='option' {{($row->id == old('type')) ? 'selected' : ''}}
                                        value='{{$row->id}}'> {{$row->label}}</option>
                                    @endforeach
                                </select>
                                    <label for="type"> Type</label>
                                    @error('type')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                    @enderror
                                </div>
                                    
                                    <div class="col s6 input-field">
                                    <select name='statut' id='statut' class="select2 browser-default">
                                    <option value=''></option>
                                    @foreach ($statutRecords as $row)
                                        <option class='option' {{($row->key == old('statut')) ? 'selected' : ''}}
                                        value='{{$row->key}}'> {{$row->value}}</option>
                                    @endforeach
                                </select>
                                    <label for="statut"> Statut</label>
                                    @error('statut')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                    @enderror
                                </div>

                                

                                       
                                                    
                                        
                                    <div class="col s6 input-field">
                                    <select name='langue' id='langue' class="select2 browser-default">
                                    <option value=''></option>
                                    @foreach ($langueRecords as $row)
                                        <option class='option' {{($row->id == old('langue')) ? 'selected' : ''}}
                                        value='{{$row->id}}'> {{$row->label}}</option>
                                    @endforeach
                                </select>
                                    <label for="langue"> Langue par défaut</label>
                                    @error('langue')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                    @enderror
                                </div>

                                 <div class="col s6 input-field">
                                            <input id="package_name" name="package_name" value="{{old('package_name')}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="package_name"> Package Name </label>
                                            @error('package_name')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>
                                                    
                                        <div class="col s6 input-field">
                                            <input id="titre" name="titre" value="{{old('titre')}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="titre"> Titre play store </label>
                                            @error('titre')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s6 input-field">
                                            <input id="subtitre" name="subtitre" value="{{old('subtitre')}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="subtitre"> Subtitre play store </label>
                                            @error('subtitre')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                        
                                        <div class="col s12 input-field">
                                            <textarea d="description" name="description" class="materialize-textarea"></textarea>
                                            <label for="description"> Description play store </label>
                                            @error('description')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        

                                        <div class="col s6 input-field">
                                            <div class="file-field input-field" style="width: 100%; float: left;">
                                                <div class="btn">
                                                    <span>Icone</span>
                                                    <input name="file" type="file">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12">
                                    <div class="col s12 display-flex justify-content-end mt-3">
                                        <a href="{{route('application_list')}}"><button type="button"
                                                class="btn btn-light">Retour </button></a>
                                        <button type="submit" class="btn indigo" style="margin-left: 1rem;">
                                            Enregistrer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop