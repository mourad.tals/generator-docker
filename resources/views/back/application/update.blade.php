@extends($layout)
@section('content')
<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">

                <form method="POST" action="{{route('application_update', ['application' => $record->id])}}" enctype="multipart/form-data">
                    @csrf
                    <br>
                    @if (Session::has('success'))
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p>{{ Session::get('success') }} </p>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    @endif

                    


                    <div class="row">
                        <div class="col s12">
                          <ul class="tabs">
                            <li class="tab col s3"><a href="#generale">Générale</a></li>
                            <li class="tab col s3"><a href="#ads">Config ADS</a></li>
                          </ul>
                        </div>

                        <div id="generale" class="col s12">
                    <div class="card" >
                        <div class="card-panel">
                        <div class="row">
                                <div class="col s12 m12">
                                    <div class="row">
                                                        
                                        <div class="col s12 input-field">
                                            <input id="label" name="label" value="{{old('label', $record->label)}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="label"> Libellé </label>
                                            @error('label')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    <div class="col s6 input-field">
                                        <select name='type' id='type' class="select2 browser-default">
                                        <option value=''></option>
                                            @foreach ($typeRecords as $row)
                                                <option class='option' {{($row->id == old('type', $record->type)) ? 'selected' : ''}}
                                                value='{{$row->id}}'> {{$row->label}}</option>
                                            @endforeach
                                        </select>
                                        <label for="type"> Type</label>
                                        @error('type')
                                            <span class="helper-text materialize-red-text">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    
                                    <div class="col s6 input-field">
                                    <select name='statut' id='statut' class="select2 browser-default">
                                    <option value=''></option>
                                        @foreach ($statutRecords as $row)
                                            <option class='option' {{($row->key == old('statut', $record->statut)) ? 'selected' : ''}}
                                            value='{{$row->key}}'> {{$row->value}}</option>
                                        @endforeach
                                </select>
                                    <label for="statut"> Statut</label>
                                    @error('statut')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                    @enderror
                                </div>



                                <div class="col s6 input-field">
                                        <select name='langue' id='langue' class="select2 browser-default">
                                        <option value=''></option>
                                            @foreach ($langueRecords as $row)
                                                <option class='option' {{($row->id == old('langue', $record->langue)) ? 'selected' : ''}}
                                                value='{{$row->id}}'> {{$row->label}}</option>
                                            @endforeach
                                    </select>
                                        <label for="langue"> Langue par défaut</label>
                                        @error('langue')
                                            <span class="helper-text materialize-red-text">{{ $message }}</span>
                                        @enderror
                                    </div>

                                                    
                                        <div class="col s6 input-field">
                                            <input id="package_name" name="package_name" value="{{old('package_name', $record->package_name)}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="package_name"> Package Name </label>
                                            @error('package_name')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    
                                                    
                                        <div class="col s6 input-field">
                                            <input id="titre" name="titre" value="{{old('titre', $record->titre)}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="titre"> Titre play store </label>
                                            @error('titre')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s6 input-field">
                                            <input id="subtitre" name="subtitre" value="{{old('subtitre', $record->subtitre)}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="subtitre"> Subtitre play store </label>
                                            @error('subtitre')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s12 input-field">
                                            <textarea d="description" name="description" class="materialize-textarea">{{old('description', $record->description)}}</textarea>
                                            <label for="description"> Description play store </label>
                                            @error('description')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>                
                                        <div class="col s6 input-field">
                                            <div class="file-field input-field" style="width: 100%; float: left;">
                                                <div class="btn">
                                                    <span>Icone</span>
                                                    <input name="file" type="file">
                                                </div>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    </div>
                     <div id="ads" class="col s12">

                        <div class="card" >
                        <div class="card-panel">
                        <div class="row">
                                <div class="col s12 m12">
                                    <div class="row">



                                        <div class="col s12">
                                            <table class="striped" id="ads_tab">
                                              <thead>
                                                <tr>
                                                    <th style="width: 20%">AD Network</th>
                                                    <th style="width: 20%">Type ADS</th>
                                                    <th style="width: 20%">Code</th>
                                                    <th>ID</th>
                                                    <th style="1%"></th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                @foreach($adsconfig as $ads)


                                                    <tr>
                                                        <td >
                                                            <select name='adnetwork[]' class=" browser-default">
                                                                <option value=''></option>
                                                                    @foreach($adnetworks as $row)
                                                                        <option class='option' value='{{$row->id}}'  {{($row->id == $ads->adnetwork) ? 'selected' : ''}}> {{$row->label}}</option>
                                                                    @endforeach
                                                                </select>
                                                        </td>
                                                        <td>
                                                            <select name='typeads[]'  class=" browser-default">
                                                                <option value=''></option>
                                                                    @foreach($typeads as $row)
                                                                        <option class='option' value='{{$row->id}}' {{($row->id == $ads->type) ? 'selected' : ''}}> {{$row->label}}</option>
                                                                    @endforeach
                                                                </select>

                                                        </td>
                                                        <td><input name="adscode[]" value="{{$ads->adscode}}" type="text"></td>
                                                        <td><input name="adsid[]" value="{{$ads->adsid}}" type="text"></td>
                                                        <td>
                                                            <i onclick="$(this).closest('tr').remove();" style="color: red; cursor: pointer;" class="material-icons delBlock" >delete_forever</i>
                                                        </td>
                                                    </tr>

                                                @endforeach

                                                @if(count($adsconfig)==0)
                                                <tr>
                                                <td >
                                                    <select name='adnetwork[]' class=" browser-default">
                                                        <option value=''></option>
                                                            @foreach($adnetworks as $row)
                                                                <option class='option' value='{{$row->id}}'> {{$row->label}}</option>
                                                            @endforeach
                                                        </select>
                                                </td>
                                                <td>
                                                    <select name='typeads[]'  class=" browser-default">
                                                        <option value=''></option>
                                                            @foreach($typeads as $row)
                                                                <option class='option' value='{{$row->id}}'> {{$row->label}}</option>
                                                            @endforeach
                                                        </select>

                                                </td>
                                                <td><input name="adscode[]" type="text"></td>
                                                <td><input name="adsid[]" type="text"></td>
                                                <td>
                                                    <i onclick="$(this).closest('tr').remove();" style="color: red; cursor: pointer;" class="material-icons delBlock" >delete_forever</i>
                                                </td>
                                            </tr>
                                            @endif
                                              </tbody>
                                            </table>
                                                    
                                            <a id="add_ads" href="#!">Ajouter</a>
                                        </div>




                                        <div class="col s6">
                                            <table class="striped">
                                              <thead>
                                                <tr>
                                                    <th></th>
                                                    @foreach($typeads as $ads)
                                                        <th>{{$ads->label}}</th>
                                                    @endforeach
                                                </tr>
                                              </thead>
                                              <tbody>
                                                @foreach($adnetworks as $nt)
                                                    <tr>
                                                        <th>{{$nt->label}}</th>

                                                        @foreach($typeads as $ads)
                                                            <td style="text-align: center;"> 
                                                                <label>
                                                                    <input type="checkbox" name="perm_ads[{{$nt->id.'_'.$ads->id}}]" {{$row->id}}' {{(in_array($nt->id.'_'.$ads->id,$adsperm)) ? 'checked' : ''}} />
                                                                    <span></span>
                                                                  </label>
                                                            </td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                              </tbody>
                                            </table>
                                        </div>

                                        


                                    </div></div></div></div></div>

                     </div>
                 </div>

                 <div class="row">
                                <div class="col s12 m12">
                                    <div class="col s12 display-flex justify-content-end mt-3">
                                        <a href="{{route('application_list')}}"><button type="button"
                                                class="btn btn-light">Retour </button></a>
                                        <button type="submit" class="btn indigo" style="margin-left: 1rem;">
                                            Enregistrer</button>
                                    </div>
                                </div>
                            </div>

                </form>
            </div>
        </div>
    </div>
</div>


<div id="ads_tr" class="hide">
    <table>
        <tbody>
        <tr>
            <td >
                <select name='adnetwork[]' class=" browser-default">
                    <option value=''></option>
                        @foreach($adnetworks as $row)
                            <option class='option' value='{{$row->id}}'> {{$row->label}}</option>
                        @endforeach
                    </select>
            </td>
            <td>
                <select name='typeads[]'  class=" browser-default">
                    <option value=''></option>
                        @foreach($typeads as $row)
                            <option class='option' value='{{$row->id}}'> {{$row->label}}</option>
                        @endforeach
                    </select>

            </td>
            <td><input name="adscode[]" type="text"></td>
            <td><input name="adsid[]" type="text"></td>
            <td>
                <i onclick="$(this).closest('tr').remove();" style="color: red; cursor: pointer;" class="material-icons delBlock" >delete_forever</i>
            </td>
        </tr>
        </tbody>
    </table>
</div>

@stop
@section('js')
<script type="text/javascript" charset="utf-8">

    $( "#add_ads" ).click(function() {
        $("#ads_tr tr").clone().appendTo($("#ads_tab"));
    });


</script>


@stop