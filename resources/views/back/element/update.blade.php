@extends($layout)
@section('content')
<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="section">

                <form method="POST" action="{{route('element_update', ['element' => $record->id])}}" enctype="multipart/form-data">
                    @csrf
                    <br>
                    @if (Session::has('success'))
                    <div class="card-alert card green">
                        <div class="card-content white-text">
                            <p>{{ Session::get('success') }} </p>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    @endif
                    <div class="card">
                        <div class="card-panel">
                        <div class="row">
                                <div class="col s12 m12">
                                    <div class="row">
                                        
                                    <div class="col s6 input-field">
                                        <input id="label" name="label" value="{{old('label', $record->label)}}" autocomplete="off"
                                        readonly onfocus="this.removeAttribute('readonly');" type="text">
                                        <label for="label"> Titre </label>
                                        @error('label')
                                            <span class="helper-text materialize-red-text">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    

                                <div class="col s6 input-field">
                                            <input id="order" name="order" value="{{old('order', $record->order)}}" autocomplete="off"
                                            readonly onfocus="this.removeAttribute('readonly');" type="text">
                                            <label for="order"> Ordre </label>
                                            @error('order')
                                                <span class="helper-text materialize-red-text">{{ $message }}</span>
                                            @enderror
                                        </div>


                                        <div class="col s6 input-field">
                                    <select name='categorie' id='categorie' class="select2 browser-default">
                                    <option value=''></option>
                                        @foreach ($categorieRecords as $row)
                                            <option class='option' {{($row->id == old('categorie', $record->categorie)) ? 'selected' : ''}}
                                            value='{{$row->id}}'> {{$row->label}}</option>
                                        @endforeach
                                </select>
                                    <label for="categorie"> Catégorie</label>
                                    @error('categorie')
                                        <span class="helper-text materialize-red-text">{{ $message }}</span>
                                    @enderror
                                </div>

                                
                                                            
                                <div class="col s6 input-field">
                                                  
                                                
                                                  <div class="file-field input-field" style="width: 100%; float: left;">
                                  <div class="btn">
                                      <span>Image</span>
                                      <input name="file" type="file">
                                  </div>
                                  <div class="file-path-wrapper">
                                      <input class="file-path" type="text">
                                  </div>
                              </div>               
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12">
                                    <div class="col s12 display-flex justify-content-end mt-3">
                                        <a href="{{route('element_list')}}"><button type="button"
                                                class="btn btn-light">Retour </button></a>
                                        <button type="submit" class="btn indigo" style="margin-left: 1rem;">
                                            Enregistrer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop