<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RefTable;
use App\Models\Droit;
use App\Models\Employe;
use App\Models\Client;
use App\Models\Ville;
use App\Models\Role;
use App\Rules\UserClient;
use App\Rules\EmployeClient;
use App\Rules\UserEmploye;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function getRules()
    {
        return [
            'name' => ['required', 'string', 'max:50'],
            'role' => ['required'],
            'last_name' => ['required', 'string', 'max:50'],
            'first_name' => ['required', 'string', 'max:50'],
            'login' => ['required', 'string', 'max:50'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }

    public function list(Request $request)
    {

        if ($request->isMethod('post')) {

            if($request->role == 0){

                $users = User::fetchAll();
                $roles = Role::fetchAll();
            }else{

                $users = User::fetchAll()->where('role', $request->role);
                $roles = Role::fetchAll();
            }

            return view(
                'back/user/list',
                [
                    'users' => $users,
                    'roles' => $roles
                ]
            );
        }
        $request->flash();
        $users = User::fetchAll();
        $roles = Role::fetchAll();

        return view(
            'back/user/list',
            [
                'users' => $users,
                'roles' => $roles
            ]
        );
    }

    public function create(Request $request)
    {


        if ($request->isMethod('post')) {
            $data = $request->all();
            $rules = [
                'name' => ['required', 'string', 'max:50'],
                'first_name' => ['required', 'string', 'max:50'],
                'login' => ['required', 'string', 'max:50'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ];

            $validator =  Validator::make($data, $rules);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            } else {

                        $user = [
                            'name' => $data['name'],
                            'first_name' => $data['first_name'],
                            'login' => $data['login'],
                            'role' => $data['role'],
                            'password' => Hash::make($data['password']),
                            'validated' => 1,
                        ];

                    }
                User::create($user);
                Redirect::to(route('user_list'))->send();
            }
        return view(
            'back/user/create',
            [
                'roles' => Role::fetchAll(),
            ]
        );
    }

    public function update(User $user, Request $request)
    {

        //'droits' =>
        if ($request->isMethod('post')) {
            $data = $request->all();
            $rules = [
                'name' => ['required', 'string', 'max:50'],
                'first_name' => ['required', 'string', 'max:50'],
                'login' => ['required', 'string', 'max:50'],
                'password' => ['confirmed'],
            ];

            $validator =  Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            } else {

                $user->first_name = $request->all()["first_name"];
                $user->name = $request->all()["name"];
                $user->login = $request->all()["login"];
                $user->role = $request->all()["role"];
                if (empty($request->get('password'))) {
                    //$data = $request->except('password');
                    //$user->update($data);
                    $user->save();
                } else {
                    //$data = $request->all();
                    $user->password = Hash::make($data['password']);
                    $user->save();
                }

                Redirect::to(route('user_list'))->send();
            }
        }

        return view(
            'back/user/update',
            [
                'roles' => Role::fetchAll(),
                'droits' => Droit::all()->where('deleted', "0"),
                'user' => $user
            ]
        );
    }

    public function delete(User $user)
    {
        $user->update(['deleted' => 1, 'deleted_at' => date("Y-m-d H:i:s"), 'deleted_by' => \Auth::user()->id]);
        return redirect()->back()->with('success', "L'enregistrement a été supprimé avec succès");
    }

    public function myProfil(Request $request)
    {

        $data = $request->all();
        $request->flash();
        $roles = Role::fetchAll();
        $rules = [
            'password' =>'confirmed|string|min:8|confirmed',

        ];
        $user = User::where('id', \Auth::user()->id)->first();

        if ($request->isMethod('post')) {
            if (strlen(trim($data['password'])) < 1) {
                unset($rules['password']);
            }
            $validator =  Validator::make($data, $rules);
            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }
            if ($user) {
                if (strlen(trim($data['password'])) > 1) {
                    $user->password = Hash::make($data['password']);
                }

               
                $user->ClientDetail->save();
                if (isset($request->file)) {
                    $fileName = time() . '.' . $request->file->extension();
                    $request->file->move(public_path('uploads/photos'), $fileName);
                    $user->photo = $fileName;
                }
                $user->save();
                $request->session()->flash('success', 'Profil modifié avec succés');
            }
        }
        return view(
            'back/user/profil',
            [
                'roles' => $roles,
                'user' => User::where('id', \Auth::user()->id)->first()
            ]
        );
    }
}
