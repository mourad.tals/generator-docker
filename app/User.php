<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Droit;
use App\Models\Ville;
use App\Models\Parameter;


class User extends Authenticatable
{
    use Notifiable;

    public function ClientDetail(){
        return $this->belongsTo(\App\Models\Client::class, 'client');
    }


    public function EmployeDetail(){
        return $this->belongsTo(\App\Models\Employe::class, 'employe');
    }


    public function droits()
    {
        return $this->belongsToMany(Droit::class, 'users_droits');
    }


    public static function fetchAll(){
        return  \DB::table("users")
            ->select("*", \DB::raw('users.id as id'), \DB::raw('roles.label as role_label'))
            ->leftJoin('roles', 'roles.id', '=', 'users.role')
            ->where('users.deleted', '0')
            ->get();
    }

    public static function hasRoute($route){
        return is_numeric(array_search($route, session('user_routes', [])));
    }

    public static function hasRessource($ressource){
        return is_numeric(array_search($ressource, session('user_ressources', [])));
    }

    public static function getRessourcesByFonctionnalites($fonctionnalites){

        $ressources = \DB::table("fonctionnalites_ressources")
                    ->select("*"
                            , \DB::raw('ressources.id as id')
                            , \DB::raw('ressources.name as ressource_name')
                            )
                    ->leftJoin('ressources', 'ressources.id', '=', 'fonctionnalites_ressources.ressource_id')
                    ->where('ressources.deleted', '0')
                    ->whereIn('fonctionnalite_id', $fonctionnalites)
                    ->get();
        return array_column($ressources->toArray(), 'ressource_name');
    }

    public static function getRoutesByFonctionnalites($fonctionnalites){

        $ressources = \DB::table("fonctionnalites_routes")
                    ->whereIn('fonctionnalite_id', $fonctionnalites)
                    ->get();
        return array_column($ressources->toArray(), 'route');
    }

    public static function getUserVilles(){
        return \Auth::user()->relatedVilles()->allRelatedIds()->toArray();
    }


    public static function getUserVillesCharger(){
        return \Auth::user()->relatedVillesCharger()->allRelatedIds()->toArray();
    }

    public static function storeUserData(){
        $role = \App\Models\Role::find(\Auth::user()->role);
        $fonctionnalites = $role->fonctionnalites()->allRelatedIds()->toArray();
        $ressources = self::getRessourcesByFonctionnalites($fonctionnalites);
        $routes = self::getRoutesByFonctionnalites($fonctionnalites);

        session(['user_ressources' => $ressources]);
        session(['user_routes' => $routes]);
        session(['global_parameters' => Parameter::find(1)]);
    }

    public function relatedVilles()
    {
        return $this->belongsToMany(Ville::class, 'villes_users', 'ville_id', 'user_id');
    }

    public function relatedVillesCharger()
    {
        return $this->belongsToMany(Ville::class, 'villes_users_charger', 'ville_id', 'user_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
