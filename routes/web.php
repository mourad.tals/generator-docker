<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::any('/admin', 'AdminController@index')->name('admin')->middleware('check-perm');


Route::any('/', 'HomeController@index')->name('home');

Route::any('/dashboard', 'AdminController@index')->name('admin')->middleware('check-perm');

//
Route::any('/user/list', 'UserController@list')->name('user_list');
Route::any('/user/create', 'UserController@create')->name('user_create');
Route::any('/user/update/{user}', 'UserController@update')->name('user_update');
Route::any('/user/delete/{user}', 'UserController@delete')->name('user_delete');
Route::any('/user/profil', 'UserController@myProfil')->name('user_profil');

// role
Route::any('/role/list', 'RoleController@list')->name('role_list');
Route::any('/role/create', 'RoleController@create')->name('role_create')->middleware('check-perm');
Route::any('/role/update/{role}', 'RoleController@update')->name('role_update');
Route::any('/role/delete/{role}', 'RoleController@delete')->name('role_delete');

// ressource
Route::any('/ressource/list', 'RessourceController@list')->name('ressource_list');
Route::any('/ressource/create', 'RessourceController@create')->name('ressource_create');
Route::any('/ressource/update/{ressource}', 'RessourceController@update')->name('ressource_update');
Route::any('/ressource/delete/{ressource}', 'RessourceController@delete')->name('ressource_delete');

// fonctionnalite
Route::any('/fonctionnalite/list', 'FonctionnaliteController@list')->name('fonctionnalite_list');
Route::any('/fonctionnalite/create', 'FonctionnaliteController@create')->name('fonctionnalite_create');
Route::any('/fonctionnalite/update/{fonctionnalite}', 'FonctionnaliteController@update')->name('fonctionnalite_update');
Route::any('/fonctionnalite/delete/{fonctionnalite}', 'FonctionnaliteController@delete')->name('fonctionnalite_delete');

// apparence
Route::any('/apparence/list', 'ApparenceController@list')->name('apparence_list');
Route::any('/apparence/create', 'ApparenceController@create')->name('apparence_create');
Route::any('/apparence/update/{apparence}', 'ApparenceController@update')->name('apparence_update');
Route::any('/apparence/delete/{apparence}', 'ApparenceController@delete')->name('apparence_delete');

// menu
Route::any('/menu/list', 'MenuController@list')->name('menu_list');
Route::any('/menu/create', 'MenuController@create')->name('menu_create');
Route::any('/menu/update/{menu}', 'MenuController@update')->name('menu_update');
Route::any('/menu/delete/{menu}', 'MenuController@delete')->name('menu_delete');