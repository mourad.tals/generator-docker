#!/bin/bash
# entrypoint.sh

# Define where we are going to be working
WORKDIR="/var/www/html"

# Create missing directories

mkdir -p ${WORKDIR}/storage/framework
mkdir -p ${WORKDIR}/storage/framework/cache
mkdir -p ${WORKDIR}/storage/framework/sessions
mkdir -p ${WORKDIR}/storage/framework/views
mkdir -p ${WORKDIR}/database/seeds
mkdir -p ${WORKDIR}/database/factories

chown -R www-data:www-data ${WORKDIR}/storage
chown -R www-data:www-data ${WORKDIR}/database

# Install dependencies
sudo -u www-data composer update -d ${WORKDIR}

# Since we need to execute artisan commands (which will also be run as www-data), we need to navigate to the working directory.
cd ${WORKDIR}

sudo -u www-data php artisan optimize
sudo -u www-data php artisan migrate
sudo -u www-data php artisan route:clear

# Continue with Apache in the foreground
apache2-foreground
 